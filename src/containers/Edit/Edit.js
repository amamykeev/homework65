import React, {Component} from 'react';
import axios from '../../axiosFirebase';


class Edit extends Component {

    state = {
        title: '',
        content: ''
    };

    valueChanged = event => {
        const name = event.target.name;
        this.setState({[name]: event.target.value});
    };

    componentDidMount() {
        axios.get('/pages/' + this.props.match.params.name  + '.json').then(response => {
            this.setState({
                title:response.data.title,
                content:response.data.content
            })
        })
    }

    componentDidUpdate(prevProps, prevState,) {
        if (this.props.match.params.name !== prevProps.match.params.name) {
            axios.get('/pages/' + this.props.match.params.name + '.json').then(response => {
                this.setState({
                    title:response.data.title,
                    content:response.data.content
                })
            })
        }
    }



    render() {
        return (
            <div className="Edit">
                <select onChange={this.valueChanged}
                        value={this.state.category}
                        name="category"
                >
                    <option>Contacts</option>
                    <option>About</option>
                </select>
                <input onChange={this.valueChanged} value={this.state.title} type="text" name="title" placeholder="title"/>
                <textarea onChange={this.valueChanged}
                          value={this.state.content}
                          name="content" placeholder="content"
                />
                <button type="button"
                        onClick={this.valueChanged}
                >Save</button>
            </div>
        );
    }
}

export default Edit;