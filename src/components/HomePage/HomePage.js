import React, {Component} from 'react';
import axios from '../../axiosFirebase';

class HomePage extends Component{

    state = {
        content: '',
        title: ''
    };

    componentDidMount() {
        axios.get('/pages/' + this.props.match.params.name + '.json').then(response => {
            this.setState({
                content:response.data.content,
                title:response.data.title
            })
        })
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        if (this.props.match.params.name !== prevProps.match.params.name) {
            axios.get('/pages/' + this.props.match.params.name + '.json').then(response => {
                this.setState({
                    content:response.data.content,
                    title:response.data.title
                })
            })
        }
    }

    render(){
        return (
            <div>
                <h1>{this.state.title}</h1>
                <p>{this.state.content}</p>
            </div>
        );
    }
};

export default HomePage;