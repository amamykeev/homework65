import React from 'react';
import NavigationItem from "./NavigationItem/NavigationItem";

const NavigationItems = () => (
    <ul className="NavigationItems">
        <NavigationItem to="/pages/home" exact>Home</NavigationItem>
        <NavigationItem to="/pages/contacts" exact>Contacts</NavigationItem>
        <NavigationItem to="/pages/about" exact>About</NavigationItem>
        <NavigationItem to="/pages/admin/edit" exact>Edit</NavigationItem>
    </ul>
);

export default NavigationItems;