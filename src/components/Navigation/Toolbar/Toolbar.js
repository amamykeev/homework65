import React from 'react';
import './Toolbar.css';
import NavigationItems from "../NavigationItems/NavigationItems";

const Toolbar = () => (
  <header className="Toolbar">
    <div>
        Logo
    </div>
      <NavigationItems/>
  </header>
);

export default Toolbar;