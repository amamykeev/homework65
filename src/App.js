import React, { Component } from 'react';
import './App.css';
import {Route, Switch} from "react-router-dom";
import HomePage from "./components/HomePage/HomePage";
import Layout from "./components/Layout/Layout";
import Edit from './containers/Edit/Edit';

class App extends Component {
  render() {
    return (
      <Layout>
        <Switch>
            <Route path="/pages/admin/edit" component={Edit}/>
            <Route path="/pages/:name"  component={HomePage}/>
        </Switch>
      </Layout>
    );
  }
}

export default App;
