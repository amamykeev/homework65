import axios from 'axios';

const instance = axios.create({
    baseURL: 'https://lesson65hwlayout.firebaseio.com/'
});

export default instance;